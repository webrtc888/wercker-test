/**
 * SkyWay Screenshare Sample App
 * @author NTT Communications(skyway@ntt.com)
 * @link https://github.com/nttcom/SkyWay-ScreenShare
 * @license MIT License
 */

$(document).ready(function () {

    //APIキー（6165842a-5c0d-11e3-b514-75d3313b9d05はlocalhostのみ利用可能）
    var APIKEY = '87958077-a9d7-41e3-91ae-04a295b51013';

    //ユーザーリスト
    var userList = [];

    //Callオブジェクト
    var existingCall;

    //localStream
    var localStream;

    // Compatibility
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    // PeerJSオブジェクトを生成
    var peer = new Peer({ key: APIKEY, debug: 3 });

    // スクリーンキャプチャの準備
    var screen = new SkyWay.ScreenShare({debug: true});

    // PeerIDを生成
    peer.on('open', function () {
        $('#my-id').text(window.location.href + "?peerid=" + peer.id);
    });

    // 相手からのコールを受信したら自身のメディアストリームをセットして返答
    peer.on('call', function (call) {
        call.answer(localStream);
        step3(call);
        console.log('event:recall');
    });

    // エラーハンドラー
    peer.on('error', function (err) {
        alert(err.message);
        step2();
    });

    // イベントハンドラー
    $(function () {
/*
        // 相手に接続
        $('#make-call').click(function () {
            makeCall();
        });

        // 切断
        $('#end-call').click(function () {
            existingCall.close();
            step2();
        });

        // メディアストリームを再取得
        $('#step1-retry').click(function () {
            $('#step1-error').hide();
            step1();
        });

        //スクリーンシェアを開始
        $('#start-screen').click(function () {
            startScreen();
        });

        //スクリーンシェアを終了
        $('#stop-screen').click(function () {
            //sc.stopScreenShare();
            localStream.stop();
        });

        //カメラ
        $('#start-camera').click(function () {
           
        });
*/
        $('#their-screen').click(function () {
            screenfull.toggle(this);
        });
        // ステップ１実行
        startCamera();
    });
/*
    function step2() {
        //UIコントロール
        $('#step1, #step3').hide();
        $('#step2').show();
    }
*/
    function step3(call) {
        // すでに接続中の場合はクローズする
        if (existingCall) {
            existingCall.close();
        }

        // 相手からのメディアストリームを待ち受ける
        call.on('stream', function (stream) {
            // 受け取ったMediaStreamのトラック数を調べる
            var _tracklengs = stream.getVideoTracks().length;
            if( _tracklengs == 2){
                // トラック数が２つの場合、Video要素に渡すための空のMediaStream objectを作成する
                // alert("tracklengs == 2");
                var _peerVideo = new webkitMediaStream();
                var _peerScreen = new webkitMediaStream();
/*
var vTrack0 = stream.getVideoTracks()[0];
alert("vtrack0 ---------");
alert("kind: " + vTrack0.kind);
alert("id: " + vTrack0.id);
alert("label: " + vTrack0.label);
var vTrack1 = stream.getVideoTracks()[1];
alert("vtrack1 ---------");
alert("kind: " + vTrack1.kind);
alert("id: " + vTrack1.id);
alert("label: " + vTrack1.label);
*/
                // 作成した２つの空のMediaStream Objectに、それぞれビデオとスクリーンシェア用のTrackを追加する
                var track = stream.getVideoTracks()[1];
                _peerScreen.addTrack(track);
                _peerVideo.addTrack(stream.getVideoTracks()[0]);
                _peerVideo.addTrack(stream.getAudioTracks()[0]);
                // それぞれのMediaStreamをVideo要素にアタッチする
                attachMediaStream($('#their-video')[0],_peerVideo);
                attachMediaStream($('#their-screen')[0],_peerScreen);
            }else {
                attachMediaStream($('#their-video')[0],stream);
            }
            //$('#step1, #step2').hide();
           // $('#step3').show();
        });

        // 相手がクローズした場合
        // call.on('close', step2);

        // Callオブジェクトを保存
        existingCall = call;

        // UIコントロール
        //$('#their-id').text(call.peer);
        //$('#step1, #step2').hide();
        //$('#step3').show();

    }

    function startCamera(){
        getUM(function(stream){
            attachMediaStream_($('#my-video')[0],stream);
            localStream = stream;
            startScreen();
        },function(error){
            //$('#step1-error').show();
        });
    }

    function startScreen(){
        if(screen.isEnabledExtension()){
        console.log("screen width: " + window.screen.width + " height: " + window.screen.height + " rate: 15");
            screen.startScreenShare({
                Width: window.screen.width, // $('#Width').val(),
                Height: window.screen.height, //$('#Height').val(),
                FrameRate: 15 //$('#FrameRate').val()
            },function (stream){
                attachMediaStream_($('#my-screen')[0],stream);
                // ビデオチャット用のMediaStreamにVideoTrackがいくつ含まれているかをチェック
                if(localStream.getVideoTracks().length == 2){
                    // 既に２つのトラックが含まれる場合は、2つめのトラック（ScreenShare）を削除
                    localStream.removeTrack(localStream.getVideoTracks()[1]);
                }
                // 取得したScreenShareのトラックからVideoトラックを取り出す
                var _SCTrack = stream.getVideoTracks()[0];
                // ビデオチャット用のMediaStreamにトラックを追加する
                localStream.addTrack(_SCTrack);
/*
alert("audio: " + localStream.getAudioTracks().length);
var vTrack0 = localStream.getVideoTracks()[0];
alert("vtrack0 ---------");
alert("kind: " + vTrack0.kind);
alert("id: " + vTrack0.id);
alert("label: " + vTrack0.label);
var vTrack1 = localStream.getVideoTracks()[1];
alert("vtrack1 ---------");
alert("kind: " + vTrack1.kind);
alert("id: " + vTrack1.id);
alert("label: " + vTrack1.label);
*/
                // 例：追加したMediaStreamを相手に送信
                var _peerid = peer.id;
                var call = peer.call(_peerid, localStream);
                step3(call);
                var sendPeerId = getPeerId();
                if (sendPeerId) {
                    makeCall(sendPeerId);
                }
            },function(error){
                console.log(error);
            },function(){
                alert('ScreenShareを終了しました');
            });
        }else{
            alert('ExtensionまたはAddonをインストールして下さい');
        }


    }

    function makeCall(peerid){
        //var call = peer.call($('#otherpeerid').val(), localStream);
        var call = peer.call(peerid, localStream);
        step3(call);
    }

    function getPeerId(){
        var arg = new Object;
        var pair=location.search.substring(1).split('&');
        for(var i=0;pair[i];i++) {
            var kv = pair[i].split('=');
            arg[kv[0]]=kv[1];
        }
        return arg["peerid"];
    }

    function getUM(success,error){
        navigator.getUserMedia({ audio: true, video: true }, function (stream) {
            success(stream);
        }, function (err) {
            error(err);
        });
    }

    function attachMediaStream_(videoDom,stream){
        // Adapter.jsをインクルードしている場合はそちらのFunctionを利用する
        if(typeof (attachMediaStream) !== 'undefined' && attachMediaStream){
            attachMediaStream(videoDom,stream);
        }else{
            videoDom.setAttribute('src', URL.createObjectURL(stream));
        }

    }
});
